package com.main.probando;

public interface Persona {

	public void gritar();
	
	public void setEdad(int edad);
	
	public int getEdad();

	public int gritando();

//	void setEdad(int edad);
	
}
