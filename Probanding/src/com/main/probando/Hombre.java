package com.main.probando;

public class Hombre implements Persona {

	private int edad;
	
	@Override
	public void gritar() {
		System.out.println("Soy hombre!");
		
	}

	public void setEdad(int edad){
		this.edad=edad;
	}

	@Override
	public int getEdad() {
		return edad;	
	}

	@Override
	public int gritando() {
		// TODO Auto-generated method stub
		return 0;
	}

}
