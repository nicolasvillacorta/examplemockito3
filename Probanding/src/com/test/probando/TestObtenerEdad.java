package com.test.probando;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.main.probando.Metodos;
import com.main.probando.Persona;

public class TestObtenerEdad {

	@Test
	public void asd() {
	
		Persona p = mock(Persona.class);
		
		p.setEdad(22);

		when(p.gritando()).thenReturn(3);
		Metodos.obtenerEdad(p);
		verify(p).gritar();
		
	}
	
	
}
